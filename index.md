---
layout: default
nofooter: true
---

Visionary, Programmer, student [MFF UK](http://mff.cuni.cz), scout at [Seda Strela](http://sedastrela.cz), ...

Find me on [Github](https://www.github.com/ondrejsika), [Flickr](https://www.flickr.com/photos/ondrejsika/), [Keybase](https://www.keybase.io/ondrejsika)

Contact me via email <ondrej@ondrejsika.com>, more contact [here](/contact.html)

My PGP key __0903EF6B__ on [pgp.mit.edu](http://pgp.mit.edu/pks/lookup?op=vindex&search=0x775D8A020903EF6B) or in file [ondrejsika_public.asc](ondrejsika_public.asc)

[index](/site-index.html) [home](/)

