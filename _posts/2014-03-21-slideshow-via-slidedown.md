---
layout: post
title: Slideshow via slidedown
categories: blog

lang: EN
meta_keywords: python, slideshow, slidedown
---

I wrote simple application __slidedown__ for easy writing HTML slideshow.

Check out here <https://github.com/ondrejsika/slidedown>.

For browsing use left and right arrows.

Live example <a href="/static/content/slideshow.html" target="_blank">here</a>.

