---
layout: post
title:  "Sublime text tips"
date:   2013-08-10 00:00:00
categories: blog

lang: EN
---

#### Searching methods & HTML tags

`ctrl/cmd + r` shows input starts with character `@`. If you will write, you search methods and functions in document. If you want search html tags, you must rewrite `@` to `#`.

#### Easy file select by name
`ctrt/cmd + p` shows list of files and input line for select file by name or path to project directory.

#### Quick move to line
`ctrl/cmd + g` shows input starts with `:` (like vim) and move cursor to specified number.

#### Multiple cursor
Start new cursor in text is very easy via `ctrl/cmd+&lt;mouse click&gt;`. If you starts new cursor in line, just `alt+shift+&lt;up&gt;/&lt;down&gt;`. If you want start cursors after all selected line, press `ctrl+shift+l`.

#### Other tips
* close file - `ctrl/cmd+w`
* select line - `ctrl/cmd+l`
* copy line - `ctrl/cmd+c`
* copy and remove line - `ctrl/cmd+x`

### Sources
<http://net.tutsplus.com/tutorials/tools-and-tips/sublime-text-2-tips-and-tricks/>