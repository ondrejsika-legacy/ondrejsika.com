---
layout: post
title: 'Nazor na zmenu propagacniho loga MFF'
categories: blog
version: 2

lang: CZ
---

Timto textem chci vyjadrit nesoulas s navrhem noveho propagacniho loga MFF UK.

> Podle me to nove logo naprosto nerespektuje tradici a prestiz fakulty. Pokud vim, tak zandny kvalitni brand (ktery bezpochyby je UK i MFF) logo nemeni. Meni logo Harvard, Apple, IBM nebo Mercedes? Tohle je opravdu strasne a smutne, ...
>
> Ondrej Sika,
> <br>ondrej@ondrejsika.com
> <br>student MFF UK

Tento text posilam jako otevreny dopis publikovany zde: <https://ondrejsika.com/blog/2015/05/19/nazor-na-zmenu-loga-mff.html>

Pokud mate zajem [vyjadrit nazor](http://novelogomff.cz/vyjadrit-nazor) a nebo si muzete precist ostatni [nazory](http://novelogomff.cz/nazory).


