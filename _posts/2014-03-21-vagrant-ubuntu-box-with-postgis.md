---
layout: post
title: Vagrant Ubuntu box with Postgis
categories: blog

lang: EN
meta_keywords: vagrant, postgres, postgis, ubuntu
---

I created Vagrant Ubuntu 12.04 32bit box with preinstalled Postgres 9.1, Postgis 2.0.

Create vagrant box

``` bash
vagrant init ondrejsika/ubuntu-postgis
```

or download box from

```
wget http://drive.ondrejsika.com/vagrant/ubuntu-postgis.box
```

