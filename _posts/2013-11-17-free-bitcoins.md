---
layout: post
title:  "Free BitCoins sites"
date:   2013-11-17 00:00:00
categories: blog

lang: EN
meta_description: List of free bitcoin sites
meta_keywords: bitcoin, free bitcoin
---

### Best sites
* [Free Bitco.in](http://freebitco.in/?r=25408)
* [BitVisitor](http://www.bitvisitor.com/?ref=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [Coin Tube](http://www.cointube.tv/?ref=1JL1hJJBNkYdrvfdD1htsguY3XGuezBw7u)
* [Bitcoin Get](http://www.bitcoinget.com)
* [I Want Free Bitcoins](http://www.iwantfreebitcoins.com/58553)
* [CoinVisitor](http://coinvisitor.com/earn.php?ref=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [Earn Free Bitcoins](http://earnfreebitcoins.com?ref=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [Bitcoin 4 You](http://www.bitcoin4you.net/?ref=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [Free BTC](https://free.btc.pt/login/?action=register&AFFID=4516)
* [Free Digital Money](http://www.freedigitalmoney.com/en/Bitcoins)
* [Rugatu](http://www.rugatu.com/)
* [Bitcoin Services](http://www.bitcoin-services.co.uk/free-bitcoins.php)


### Half Hourly Sites

* [Can Has Bitcoin?](http://www.canhasbitcoin.com/)
* [BTC 4 You](http://btc4you.com/)
* [Faucet BTC](http://www.faucetbtc.com/)
* [Free Bitcoins](http://freebitcoins.me/)
* [The Free Bitcoins](http://www.thefreebitcoins.com/)
* [Virtual Faucet](http://www.virtualfaucet.com/)
* [Free BTC 4 All](http://www.freebtc4all.com/)


### Hourly Sites

* [CoinAdd](https://coinad.com?r=14757)
* [Daily Bitcoins](http://dailybitcoins.org/index.php?aff=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [MMO Club](http://mmoclub.com/?id=88694)


### Daily Sites

* [NetLookup](http://netlookup.se/free-bitcoins/410075)
* [BitHits](http://www.bithits.info/index.php?ref=1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti)
* [BitCrate](http://www.bitcrate.net/index.php?ref=1728)


### One Time Sites

* [Bitcoin Faucet](http://faucet.bitcoin.st/)
* [FreeCoins](https://freecoins.herokuapp.com/)
* [LegendaryFaucet](http://legendaryfaucet.herokuapp.com)

You may donate me at `1sikay2CvWNryyF1SJAo6YLmd5e1D36Ti`
