---
layout: post
title: Piána na ulici
categories: blog

lang: CZ
---

Už delší dobu chodím po Hlavním vlakovém nádraží v Praze a slyším nádherný zvuk piana z akce [Piana na ulici](http://piananaulici.cz).

Je to velmi zajímavý a krásný nápad. Mnohem lepší než všechny ty kontajnery k Světu.

Den je hned lepší, když cestou do práce mohu poslouchat drobné živé konzerty.

<iframe width="420" height="315" src="//www.youtube.com/embed/vCtqFpXpz0c" frameborder="0" allowfullscreen></iframe>

<br>

### Podpořte piána

Piána můžete podpořit na českém kickstarteru HitHit <https://www.hithit.com/cs/project/204/piana-na-ulici?variant=b>

Více informací na webu <http://piananaulici.cz>, facebooku <http://facebook.com/piananaulici>, emailu __piananaulici@email.cz__ a telefonu __+420 775 224 086__.