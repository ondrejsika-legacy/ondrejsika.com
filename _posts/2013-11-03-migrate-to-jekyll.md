---
layout: post
title:  "Migrate to Jekyll"
date:   2013-11-03 16:50:53
categories: blog

lang: EN
---

My site migrate from [flask-server][flask-server] to [jekyll][jekyll].

[flask-server]: https://github.com/sikaondrej/flask-server
[jekyll]:    http://jekyllrb.com
