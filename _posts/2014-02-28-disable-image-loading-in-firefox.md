---
layout: post
title: Disable image loading in Firefox
categories: blog

lang: EN
meta_keywords: image loading, firefox
---

Go to url `about:config` and change `permission.default.image` to `2`. Revert to default, set `1`.
