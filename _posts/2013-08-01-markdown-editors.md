---
layout: post
title:  "Markdown editors"
date:   2013-08-01 00:00:00
categories: blog

lang: EN
---

I would like to introduce you to some markdown editors, which according to me, it's good fo knowing

### Light weight web editor

This web markown editor is pretty for its simplicity. Author: [James Taylor](https://github.com/jbt). Source on [GitHub](https://github.com/jbt/markdown-editor). Special feature is that, markdown file (encoded by base64) saves to url after `#`

#### example
[http://jbt.github.io/markdown-editor/](http://jbt.github.io/markdown-editor/#bVPLbtswELzzK7ZwANmtLfWcntqkaQMkQNHHKSgQWqQkWiJXIFdWkqL/3l0qcHoIYIIGOZzZ2VmtYP3F0ddpv7sa9BGjNRu41bE3OAf4bBxhVOqTTq6GKdlmGqCxmqZoYXCJzpWCt3BBcaguvIF38AMIIemjBeosNG6wAriMugUdDJiII+h8Di4wtLPMxPuA2oAjAV/JZY2BbKAEmu+Fj29D5vz1/QYSwiNOUOsAqROEECal1HXhISC0iAY0wRwdudAygR+ZtBLcYCOQfaCtsLSYMSyB3lInWCaOyQ5NqdQNYr/lcsXpm+y0QZRtr+OyPSn1kW2JiyJlEq7cWAbf398f9FGnOrqR1Nm6mUJNDsN680cBnK0L447FpuzID+viGrRnHT4qi80H9ZcXv1fqZ+cS8O8OAywp/V53RGM6r6rWUTftyxp9ddhT5Z8z29mc2UbcDZaAK+oDzuAauC44lv372EszKVc7S+XsVH3jYJIEcRtLuETc57hE/k4MwcK6qLO4j4YxWXyMeLA1pUpwz+rVZgtNRA9z5+pOSWwupNFFLS0QGe512maN3LSO/z2Ck5Q8x77AOA5iUK29lLhareAHTU0jc2iEw+veZqJlCu+kBda82qG6Oxw2nF58me1Rx8R555cXXPqti/E/h+LG56MyWKqWx2JEzzaXnB65zodd59pu4EVc02I+M56Oy0M6cSZsaOZx9To4XacSY5vPqhO6suFZaqGH042MJn8BONE4UR4y2A9Y9ynLHdLO2GbQZF+1b3TosdWuesEtKu2TG0ehxgaMJn1qK89Hw4sV+XtL6h8=)


### Native markdown editor for MAC

It's very similar to jbt's markdown editor, but this is native for MAC. Project is on <http://mouapp.com/>


### Web extended editor (IDE)

[Markable.in](http://markable.in/) is good markdown IDE. You may have account and manage your files online. Disadvantage is low speed of pretty preview.


### Web editor works with GitHub, Google Drive, Dropbox

This editor [Dillinger](http://dillinger.io/), can be connect with your data host and editing markdown files remote (on web), with are synce by Dropbox to local directories.