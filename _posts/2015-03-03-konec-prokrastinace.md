---
layout: post
title: 'Otevreny dopis pro Petra Ludwiga: Konec Prokrastinace'
categories: blog
version: 2

lang: CZ
---

Dobdry den,

prave jsem docetl Vasi knihu konec prokrastinace. Cetl jsem ji 2 dny (to se mi obvykle nestava), skvele se cte a hrozne na me zapusobila.

Drive jsem cetl "Mnich, ktery prodal sve ferrary" a hodne principu to ma spolecnych, ale Vase podani me pripada lepsi pro pouziti v praxi.

Hned dalsi den, jsem videl vyraznou zmenu na svem zivote, moc Vam timto dekuji.

Na konci knihy mate prosbu o zaslani jedne myslenky, kterou si muzu zapamatovat a ostatni zapomenu. Na me nejvice zapusobila veta ze strany 21, cituji: "... lide vice lituji toho co ve svem zivote neudelali, nez toho, co udelali."

S pozdravem,
<br>Ondrej Sika

Tento text posilam jako otevreny dopis publikovany zde: <https://ondrejsika.com/blog/2015/03/03/konec-prokrastinace.html>

