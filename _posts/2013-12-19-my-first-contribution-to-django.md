---
layout: post
title:  "My first contribution to Django"
categories: blog

lang: EN
---

Commit <https://github.com/django/django/commit/250aea2afe40ed13ee98175be628a6b15b2362ed>

Pull request <https://github.com/django/django/pull/2092>