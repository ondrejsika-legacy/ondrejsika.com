---
layout: post
title:  "Czech hackathon 2013"
date:   2013-11-20 00:00:00
categories: blog

lang: EN
meta_keywords: Czech Hackathon, czechhackathon.cz
---

### Video

<iframe width="640" height="360" src="//www.youtube.com/embed/GjRqOsH05JU" frameborder="0" allowfullscreen></iframe>
<p></p>


### Zoorilla (our project)

* Responsive web-based client for Apache Zookeeper
* Project on GitHub <http://github.com/zoorilla>
* Zoorilla client <http://zoorilla.github.io>


### Other informations

* [czechhackathon.cz](http://czechhackathon.cz)
* [Kerio Blog](http://www.kerio.com/blog/czech-hackathon-2013-in-two-minutes)
