---
layout: post
title:  "MCabber - Terminal XMPP Client"
categories: blog

lang: EN
meta_description: Basic usage of MCabber.
meta_keywords: MCabeer, xmpp, terminal
---

### Installation

On Debian from package

```
apt-get install mcabber
```

### Login

run

```
mcabber
```

and type

```
/set jid = sika.ondrej@gmail.com
/set password = mypassword
/connect
```

### Basic controll

```
PgUp - move up in contact lists
PgDn - move down in contact lists

/quit - exit mcabber
```

### Config file

You may use config in `~/.mcabber/mcabberrc`

Into config type commands as to mcabber console (without slash)

Example:

```
set jid = sika.ondrej@gmail.com
```
