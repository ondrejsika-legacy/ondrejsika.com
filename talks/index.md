---
layout: default
title: Talks
---

## Talks

<table>

<tr id="bitcoin-pilsen-2015"><td>
<b>Uvod do Bitcoinu (Pilsen, Czech Rep.)</b>
</td><td>
26/08/2015
</td><td>
<a href="https://www.facebook.com/events/1587922088138594/">Facebook</a>
</td><td>
<a href="https://speakerdeck.com/ondrejsika/uvod-do-bitcoinu-plzen">SpeakerDeck</a>
</td><td>
<a href="https://speakerd.s3.amazonaws.com/presentations/fe5a08e0c75248ffbcf6bf914b3dad79/Ondrej_Sika__Bitcoin_Intro__slides.pdf">PDF</a>
</td><td>
<a href="https://github.com/ondrejsika/uvod-do-bitcoinu-slides">GIT</a>
</td></tr>

<tr id="bitcoin-bern-2015"><td>
<b>MergedMining, SatoshiLabs (Bern, Switzerland)</b>
</td><td>
19/08/2015
</td><td>
<a href="http://www.meetup.com/Bitcoin-Meetup-Switzerland/events/224257387/">Meetup.com</a>
</td><td>
<a href="https://speakerdeck.com/ondrejsika/merged-mining-satoshilabs-bern">SpeakerDeck</a>
</td><td>
<a href="https://speakerd.s3.amazonaws.com/presentations/278e757a848a4e4480a456e90137fbd7/Ondrej_Sika__Merged_Mining_Bern__slides.pdf">PDF</a>
</td><td>
<a href="https://github.com/ondrejsika/merged-mining-bern-slides">GIT</a>
</td></tr>

<tr><td>
<b>Merged Mining a Sidechains (Bitcoin Meetup)</b>
</td><td>
25/05/2015
</td><td>
</td><td>
<a href="https://speakerdeck.com/ondrejsika/merged-mining-a-sidechains-bitcoin-meetup">SpeakerDeck</a>
</td><td>
<a href="https://speakerd.s3.amazonaws.com/presentations/667b7f7262834fd3bb2b45e493b822ce/merged_mining_sidechains.pdf">PDF</a>
</td><td>
</td></tr>

<tr id="ctjb-2015"><td>
<b>Merged Mining (CTJB)</b>
</td><td>
02/05/2015
</td><td>
</td><td>
<a href="https://speakerdeck.com/ondrejsika/merged-mining-ctjb">SpeakerDeck</a>
</td><td>
<a href="http://drive.ondrejsika.com/talks/2015/merged-mining-ctjb/Ondrej_Sika__Merged_Mining__slides.pdf">PDF</a>
</td><td>
<a href="https://github.com/ondrejsika/merged-mining-slides/tree/ctjb">GIT</a>
</td></tr>

</table>

<script>
if(window.location.hash) {
    hash = window.location.hash.slice(1);
    row = document.getElementById(hash);
    row.style.backgroundColor = '#DDDDDD';
}
</script>


{% comment %}
### Topics

##### CZ

* [GIT pro začátečníky](git-pro-zacatecniky-cz.html)
* [Úvod do Pythonu](uvod-do-pytonu-cz.html)
{% endcomment %}

