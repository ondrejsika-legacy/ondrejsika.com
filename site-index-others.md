---
layout: default
title: Others
---


## {{page.title}}


* [articles](/articles/)
* [books](/books/)
* [PF](/pf/)
* [old blog](old-blog.html)
* [my quotes](others/quotes.html)
* [git setup](others/git.html)
* [example design](others/design.html)
* [my](/my/)

