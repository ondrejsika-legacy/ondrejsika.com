---
layout: default
title: Contact
---


## {{page.title}}

* email: __<ondrej@ondrejsika.com>__ (preferred)
* skype: __ondrejsika__
* xmpp/jabber: __ondrej@ondrejsika.com__
* phone: __+420 773 452 376__ (Czech), __+41 78 753 21 62__ (Swiss)
* phone sip: __+420 226 531 633__ (__sika@xphone.cz__)

### Address

Contact address

    Molakova 544/34
    flat no. 818
    180 00 Prague
    Czech Republic

[open street map](http://www.openstreetmap.org/node/296801697)


Pernament address

    Klatovska 1532/71
    301 00 Pilsen
    Czech Republic

[open street map](http://www.openstreetmap.org/node/296689680)


### Billing information

[fakturacni udaje](/fakturacni-udaje.html)

* ID Number: __88114163__
* Tax ID Number: __CZ9302252102__


### Datova schranka

* id: __dcd68s8__


### Bank account

* Payments in Czech Republic (CZK): __2500171198 / 2010__
* Payments in Slovakia (EUR): __2500171198 / 8330__
* IBAN: __CZ0620100000002500171198__
* BIC code / SWIFT: __FIOBCZPPXXX__
* Bank address: __Fio banka, a.s., V Celnici 1028/10, Praha 1, Czech Republic__

I also accept __Bitcoin__, [my addresses](/ba.html).


