---
layout: default
title: Site Index
---

## {{page.title}}

* [edu](/edu/)
* [resume (pdf)](/resume.pdf) [(source)](https://github.com/ondrejsika/resume)
* [projects](/projects/)
* [software](/software/)
* [talks](/talks/)
* [contact](/contact.html)
* [bitcoin addresses](/ba.html)
* [ssh keys](/ssh.html)
* [drive](http://drive.ondrejsika.com/)
* [others](/site-index-others.html)

My related sites

* [article](https://ar.os1.cz)
* [books](https://books.os1.cz)
* [prints](https://prints.os1.cz)

