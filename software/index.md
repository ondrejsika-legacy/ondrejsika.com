---
layout: default
title: My Software
---

## {{ page.title }}

### Tools & Utils

* [Bitcoin Block Parser](https://ondrejsika.com/js-utils/bitcoin-block-parser.html) - parse Bitcoin block header in hex format
* [Light Block Explorer (lbe)](https://github.com/ondrejsika/lbe) - simple block explorer requires only Xcoind RPC interface
* [paste](https://github.com/ondrejsika/paste) - Opensource Paste Service
* [rajce_download](https://github.com/ondrejsika/rajce_download) - batch download from rajce.net
* [TexB](https://github.com/ondrejsika/texb) - util to easy build latex documents

need docs

* [massresize](https://github.com/ondrejsika/massresize) - resize multiple images - needs readme
* [pysendmail](https://github.com/ondrejsika/pysendmail) - send email by some smtp server - needs tsl arg

